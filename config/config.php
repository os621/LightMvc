<?php

// 系统配置
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'sys_config.php';

$config->debug = true;

$config->db['host'] = '127.0.0.1';

$config->db['db'] = '';

$config->db['user'] = '';

$config->db['pass'] = '';

// 是否打开memcached缓存,需要php扩展memcached
$config->memcached['on'] = false;

// memcached Host
$config->memcached['host'] = '127.0.0.1';

// memcached Host
$config->memcached['port'] = 3306;

// memcached 过期时间 秒
$config->memcached['exps'] = 2;

// 是否打开Smarty缓存
$config->Smarty['cached'] = true;

// Smarty缓存时间间隔
$config->Smarty['cache_lifetime'] = 2;

// 系统根目录
$config->shoproot = '/';

// 系统根域名 /结尾
$config->domain = 'http://www.iwshop.cn/';